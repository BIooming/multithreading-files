package com.epam.tc.task33.ioprocessor;

import com.epam.tc.task33.controller.BoundedOperationsBuffer;
import com.epam.tc.task33.controller.ThreadController;
import com.epam.tc.task33.entity.Operation;
import com.epam.tc.task33.entity.OperationName;
import org.apache.log4j.Logger;

import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by blooming on 6.1.16.
 */
public class IOProcessor extends Thread {
    private static final String OUTPUT_FILE = "out.dat";
    private static final Logger logger = Logger.getLogger("file");
    private static final Pattern FILENAME_PATTERN =
            Pattern.compile("(in_<\\d+>\\" + ".dat)");

    private ThreadController threadController;
    private Matcher matcher;
    private Path path;
    private boolean running = true;

    public IOProcessor(String path, ThreadController threadController) {
        this.path = Paths.get(path);
        this.threadController = threadController;
    }

    @Override
    public void run() {
        try {
            readFromDirectory();

            while (running) {
                sleep(100);
            }
        } catch (InterruptedException e) {
            logger.error(e);
        }
    }

    public void terminate() {
        running = false;
    }

    public void writeResult(Double result) {
        try {
            FileWriter fileWriter = new FileWriter(path + "/" + OUTPUT_FILE);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write(result.toString());
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readFromDirectory() {
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
            for (Path file : stream) {
                String fileName = file.getFileName().toString();

                matcher = FILENAME_PATTERN.matcher(fileName);
                if (matcher.matches()) {
                    Operation operation = getOperationFromFile(file.toFile());
                    BoundedOperationsBuffer.getInstance().put(operation);
                }
            }

            threadController.allFilesReadNotify();
        } catch (IOException | DirectoryIteratorException e) {
            logger.error(e);
        } catch (InterruptedException e) {
            logger.error(e);
        }
    }

    private Operation getOperationFromFile(File file) {
        Operation operation = new Operation();

        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String num = bufferedReader.readLine();
            Integer operationNum = Integer.parseInt(num);
            operation.setOperationName(OperationName.getOperationName
                    (operationNum));

            String[] values = bufferedReader.readLine().split("\\s");
            List<Double> valueList = new ArrayList<Double>();

            for (String value : values) {
                valueList.add(Double.parseDouble(value));
            }
            operation.setNumbers(valueList);

        } catch (FileNotFoundException e) {
            logger.error(e);
        } catch (IOException e) {
            logger.error(e);
        } catch (EnumConstantNotPresentException e) {
            logger.error("Invalid operation number in file");
        }

        return operation;
    }
}
