package com.epam.tc.task33.controller;

import com.epam.tc.task33.entity.Operation;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by blooming on 10.1.16.
 */
public class BoundedOperationsBuffer {
    private static final BoundedOperationsBuffer instance = new BoundedOperationsBuffer();

    private final Lock lock = new ReentrantLock();
    private final Condition notFull  = lock.newCondition();
    private final Condition notEmpty = lock.newCondition();

    private final Operation[] operations = new Operation[100];
    private int putPointer, takePointer, count;
    private boolean elementAddingFinished = false;

    private BoundedOperationsBuffer(){
    }

    public static BoundedOperationsBuffer getInstance(){
        return instance;
    }

    public void put(Operation x) throws InterruptedException {
        lock.lock();
        try {
            while (count == operations.length) {
                notFull.await();
            }
            operations[putPointer] = x;
            if (++putPointer == operations.length){
                putPointer = 0;
            }
            ++count;
            notEmpty.signal();
        } finally {
            lock.unlock();
        }
    }

    public Operation take() throws InterruptedException {
        lock.lock();
        try {
            while ((count == 0) && (!elementAddingFinished)) {
                notEmpty.await();
            }
            Operation x = operations[takePointer];
            if (++takePointer == operations.length){
                takePointer = 0;
            }
            --count;
            notFull.signal();
            return x;
        } finally {
            lock.unlock();
        }
    }

    public int getCount(){
        return count;
    }
}
