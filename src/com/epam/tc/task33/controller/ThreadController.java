package com.epam.tc.task33.controller;

import com.epam.tc.task33.ioprocessor.IOProcessor;
import com.epam.tc.task33.logic.OperationProcessor;
import org.apache.log4j.Logger;

import java.util.concurrent.Phaser;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by blooming on 6.1.16.
 */
public class ThreadController extends Thread {
    private final static Logger logger = Logger.getLogger("file");
    private boolean allFilesRead = false;

    private ReentrantLock lock = new ReentrantLock();
    private Phaser phaser = new Phaser();
    private IOProcessor ioProcessor;
    private Double result = 0d;

    private ThreadController() {
    }

    public ThreadController(String workPath) {
        ioProcessor = new IOProcessor(workPath, this);
    }

    @Override
    public void run() {
        ioProcessor.start();

        while (!allFilesRead) {
            getOperationsFromBuffer();
        }
        awaitOperationsToBePerformed();

        ioProcessor.writeResult(result);
        ioProcessor.terminate();
    }

    private void awaitOperationsToBePerformed() {
        try {
            while (phaser.getRegisteredParties() != 0) {
                sleep(10);
            }
        } catch (InterruptedException e) {
            logger.error(e);
        }
    }

    private void getOperationsFromBuffer() {
        try {
            phaser.register();
            OperationProcessor operationProcessor =
                    new OperationProcessor(BoundedOperationsBuffer.getInstance().take(),
                            phaser, this);
            operationProcessor.start();
        } catch (InterruptedException e) {
            logger.error(e);
        }
    }

    public void allFilesReadNotify() {
        allFilesRead = true;

        while (BoundedOperationsBuffer.getInstance().getCount() > 0) {
            getOperationsFromBuffer();
        }
    }

    public void addResult(Double operationResult) {
        lock.lock();
        result = result + operationResult;
        lock.unlock();
    }
}