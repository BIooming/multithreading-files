package com.epam.tc.task33.start;

import com.epam.tc.task33.controller.ThreadController;
import org.apache.log4j.Logger;

public class Start {
    private final static Logger logger = Logger.getLogger("file");

    public static void main(String[] args) {
        ThreadController threadController = new ThreadController(args[0]);
        threadController.start();
        try {
            threadController.join();
        } catch (InterruptedException e) {
            logger.error(e);
        }
    }
}
