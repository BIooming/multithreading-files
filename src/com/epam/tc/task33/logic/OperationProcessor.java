package com.epam.tc.task33.logic;

import com.epam.tc.task33.controller.ThreadController;
import com.epam.tc.task33.entity.Operation;

import java.util.concurrent.Phaser;

/**
 * Created by blooming on 6.1.16.
 */
public class OperationProcessor extends Thread {

    private static final double SQUARE = 2d;

    private double result = 0d;
    private Operation operation;
    private ThreadController controller;
    private Phaser phaser;

    public OperationProcessor(Operation operation, Phaser phaser,
                              ThreadController controller) {
        this.operation = operation;
        this.controller = controller;
        this.phaser = phaser;
    }

    private void doWork() {
        switch (operation.getOperationName()) {
            case ADDING:
                for (Double value : operation.getNumbers()) {
                    result = result + value;
                }
                break;
            case MULTIPLICATION:
                result = 1d;
                for (Double value : operation.getNumbers()) {
                    result = result * value;
                }
                break;
            case SUMOFSQUARES:
                for (Double value : operation.getNumbers()) {
                    result = result + Math.pow(value, SQUARE);
                }
        }
        controller.addResult(result);
        phaser.arriveAndDeregister();
    }

    @Override
    public void run() {
        doWork();
    }
}
