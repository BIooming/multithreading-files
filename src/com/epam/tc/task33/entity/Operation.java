package com.epam.tc.task33.entity;

import java.util.List;

/**
 * Created by blooming on 6.1.16.
 */
public class Operation {
    private List<Double> numbers;
    private OperationName operationName;

    @Override
    public String toString() {
        return "Operation@" +
                "numbers=" + numbers +
                ", operationName=" + operationName;
    }

    @Override
    public boolean equals(Object obj) {
        if (null == obj){
            return false;
        }
        if (this == obj){
            return true;
        }
        if (!(obj instanceof Operation)){
            return false;
        }

        Operation operation = (Operation) obj;
        if (!numbers.equals(operation.numbers)) {
            return false;
        }
        if(!operationName.equals(operation.operationName)){
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = numbers.hashCode();
        result = 31 * result + operationName.hashCode();
        return result;
    }

    public List<Double> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<Double> numbers) {
        this.numbers = numbers;
    }

    public OperationName getOperationName() {
        return operationName;
    }

    public void setOperationName(OperationName operationName) {
        this.operationName = operationName;
    }
}
