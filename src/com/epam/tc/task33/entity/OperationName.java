package com.epam.tc.task33.entity;

/**
 * Created by blooming on 6.1.16.
 */
public enum OperationName {
    ADDING, MULTIPLICATION, SUMOFSQUARES;

    public static OperationName getOperationName(int number) throws
            EnumConstantNotPresentException {
        switch (number) {
            case 1:
                return ADDING;
            case 2:
                return MULTIPLICATION;
            case 3:
                return SUMOFSQUARES;
            default:
                throw new EnumConstantNotPresentException(OperationName.class,
                        Integer.toString(number));
        }
    }
}
